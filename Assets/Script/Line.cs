﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Line : MonoBehaviour
{
    Coroutine drawing;
    public float lineWidth = 0.2f;
    public Color lineColor = Color.white;
    public bool changeColor = false;
    public List<GameObject> lines = new List<GameObject>();

      public bool delete = false;
    public bool red = false;
    public bool green = false;
    public bool blue = false;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartLine();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            FinishLine();
        }
        
        if (changeColor)
        {
            UpdateLineColor();
        }
        if (Input.GetKeyDown(KeyCode.R) || (green == true))
        {
            lineColor = Color.green;
            changeColor = true;
        }
        if (Input.GetKeyDown(KeyCode.T)|| (blue == true))
        {
             Debug.Log("Tekan Biru");
            lineColor = Color.blue;
            changeColor = true;
        }
        

        if (Input.GetKeyDown(KeyCode.Delete) || delete == true)
        {
            Debug.Log("Tekan");
            delete = false;
            DeleteLastLine();
        }
    }

    void StartLine()
    {
        if (drawing != null)
        {
            StopCoroutine(drawing);
        }
        GameObject lineObject = Instantiate(Resources.Load("Line") as GameObject, new Vector3(0, 0, 0), Quaternion.identity);
        
        lines.Add(lineObject);
        
        drawing = StartCoroutine(DrawLine(lineObject));
    }

    void FinishLine()
    {
        StopCoroutine(drawing);
    }

    IEnumerator DrawLine(GameObject lineObject)
    {
        LineRenderer line = lineObject.GetComponent<LineRenderer>();
        line.positionCount = 0;
        line.startWidth = lineWidth;
        line.endWidth = lineWidth;
        line.startColor = lineColor;
        line.endColor = lineColor;

        while (true)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0;
            line.positionCount++;
            line.SetPosition(line.positionCount - 1, position);
            yield return null;
        }
    }

     void YourFunctionToExecute()
    {
        // Implement your function logic here
    }

        void UpdateLineColor()
        {
            GameObject lineObject = GameObject.Find("Line");
            if (lineObject != null)
            {
                LineRenderer line = lineObject.GetComponent<LineRenderer>();
                line.startColor = lineColor;
                line.endColor = lineColor;
            }
        }

    void DeleteLastLine()
    {
        if (lines.Count > 0)
        {
            GameObject lastLine = lines[lines.Count - 1];
            lines.Remove(lastLine);
            Destroy(lastLine);
        }
    }


    public void tombolHapus()
    {
        delete = true;
        red = false;
        blue = false;
        green = false;
    }
    public void warnaMerah()
    {
        red = true;
        blue = false;
        green = false;
        delete = false;
    }
    public void warnaBiru()
    {
        blue = true;
        green = false;
        red = false;
        delete = false;

         lineColor = Color.green;
            changeColor = true;
    }
    public void warnaHijau()
    {
        green = true;
           blue = false;
        red = false;
        delete = false;
    }
}
